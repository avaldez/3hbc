@extends('layouts.app')

@section('content')
<div id="row">

<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Path</th>
      <th scope="col">Endpoint</th>
      <th scope="col">Fecha</th>
    </tr>
  </thead>
  <tbody>

@foreach ($result as $val)


    <tr>
      <th scope="row">{{$val->id }}</th>
      <td>{{$val->path}}</td>
      <td>{{$val->enpoint}}</td>
      <td>{{$val->created_at}}</td>
    </tr>
    
  

  @endforeach
  </tbody>
</table>
</div>

@endsection
