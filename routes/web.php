<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Auth::routes();
Route::group(['middleware' => ['auth']], function (){
    
    Route::group(['prefix' => '/logs'], function(){
        Route::get('/',     'LogsController@index')->name('ilogs');
        Route::post('/{criterio}',     'LogsController@index')->name('blogs');
        Route::post('/add', 'LogsController@store')->name('alogs');
        Route::delete('/del/{id}', 'LogsController@destroy')->name('dlogs');
    });


});
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('home');


