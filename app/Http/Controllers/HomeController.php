<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use App\logs;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $url = "https://api.github.com/orgs/octokit/repos";

        $response   = Curl::to($url)
        ->withData( array() )
        ->withHeader( "Accept: application/vnd.github.v3+json" )
        ->withOption( 'USERAGENT', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13' )
        //->asJson()         
        ->get();
        /*dump($response);
        die();*/

        logs::add(array('path' => 'orgs/octokit/repos', 'response' => $response, 'enpoint' => $url ));
        $result=json_decode($response);
        //dump($result[0]);

        //return view('home');
        return view('home')->with(array("result" => $result));
    }
}
