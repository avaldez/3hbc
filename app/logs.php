<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class logs extends Model
{
    protected $table = 'logs';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','path','enpoint','response',
    ];


    protected function get($id = false)
    {
        if($id)
        {
                $result = logs::find($id);    
        }
        else
        {
                $result = logs::all();    
        }
        return $result;
    }

    
    protected function add($dat)
    {
        $reg = new logs;
        $reg->path                  = $dat['path'];
        $reg->enpoint               = $dat['enpoint'];    
        $reg->response              = $dat['response'];    
        $reg->save();
        return $reg->id;
    }
    
    
    protected function updateL($dat)
    {
         
        $result = logs::where('id', $dat->id)->update(['path' => $dat->path]);    
        if($result > 0)
        {
                return $result;
        }
        else
        {
                return false;
        }
    }
         
    
    
    protected function del($id)
    {
        if(logs::where('id', $id)->delete() > 0)
        {
                return true;
        }
        else
        {
                return false;
        }
    }

}
