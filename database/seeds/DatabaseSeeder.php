<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
    }
    
}

class UsersTableSeeder extends Seeder {

    public function run()
    {
            DB::table('users')->insert(array(
            'name'          => 'admin',
            'email'         => 'admin@3hbc.com',
            'password'      => Hash::make('admin$'.date('Y')),
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
            ));

            DB::table('users')->insert(array(
                'name'          => 'abraham',
                'email'         => 'abraham.valdez4@gmail.com',
                'password'      => Hash::make('abraham$'.date('Y')),
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
                ));

    }
}